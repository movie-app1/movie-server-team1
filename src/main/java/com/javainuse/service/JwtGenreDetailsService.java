package com.javainuse.service;

import com.javainuse.repository.GenreRepository;
import com.javainuse.model.GenreModel;
import com.javainuse.DTO.GenreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtGenreDetailsService {

    @Autowired
    private GenreRepository genreDao;
    private long id;

    public JwtGenreDetailsService() {
    }

    public GenreModel save(GenreDTO product) {

        GenreModel newGenre = new GenreModel();
        newGenre.setGenre(product.getGenre());


        return genreDao.save(newGenre);
    }


    public Optional<GenreModel> findById(Long id) {
        return Optional.ofNullable(genreDao.findById(id));
    }


    public List<GenreModel> findAll() {
        List<GenreModel> genres = new ArrayList<>();
        genreDao.findAll().forEach(genres::add);
        return genres;
    }


    public void delete(Long id) {
        GenreModel genre = genreDao.findById(id);
        genreDao.delete(genre);
    }


    public GenreModel update(Long id) {
        GenreModel genre = genreDao.findById(id);
        return genreDao.save(genre);
    }

}