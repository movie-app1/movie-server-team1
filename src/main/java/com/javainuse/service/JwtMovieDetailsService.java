package com.javainuse.service;

import com.javainuse.repository.MovieRepository;
import com.javainuse.model.MovieModel;
import com.javainuse.DTO.MovieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtMovieDetailsService {

    @Autowired
    private MovieRepository movieDao;
    private long id;

    public JwtMovieDetailsService() {
    }

    public MovieModel save(MovieDTO movie) {

        MovieModel newMovie = new MovieModel();
        newMovie.setTitle(movie.getTitle());
        newMovie.setSutradara(movie.getSutradara());
        newMovie.setDescription(movie.getDescription());
        newMovie.setCover(movie.getCover());
        newMovie.setTrailer(movie.getTrailer());
        newMovie.setRealease(movie.getRealease());
        newMovie.setRating(movie.getRating());
        newMovie.setGenre(movie.getGenre());

        return movieDao.save(newMovie);
    }


    public Optional<MovieModel> findById(Long id) {
        return Optional.ofNullable(movieDao.findById(id));
    }


    public List<MovieModel> findAll() {
        List<MovieModel> movies = new ArrayList<>();
        movieDao.findAll().forEach(movies::add);
        return movies;
    }


    public void delete(Long id) {
        MovieModel movie = movieDao.findById(id);
        movieDao.delete(movie);
    }


    public MovieModel update(Long id) {
        MovieModel movie = movieDao.findById(id);
        return movieDao.save(movie);
    }

}