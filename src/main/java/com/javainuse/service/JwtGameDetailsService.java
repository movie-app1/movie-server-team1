package com.javainuse.service;

import com.javainuse.DTO.GameDTO;
import com.javainuse.model.GameModel;
import com.javainuse.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class JwtGameDetailsService {

    @Autowired
    private GameRepository gameDao;
    private long id;

    public JwtGameDetailsService() {
    }

    public GameModel save(GameDTO game) {

        GameModel newGame = new GameModel();
        newGame.setGame(game.getGame());
        newGame.setImage(game.getImage());


        return gameDao.save(newGame);
    }


    public Optional<GameModel> findById(Long id) {
        return Optional.ofNullable(gameDao.findById(id));
    }


    public List<GameModel> findAll() {
        List<GameModel> games = new ArrayList<>();
        gameDao.findAll().forEach(games::add);
        return games;
    }


    public void delete(Long id) {
        GameModel game = gameDao.findById(id);
        gameDao.delete(game);
    }


    public GameModel update(Long id) {
        GameModel game = gameDao.findById(id);
        return gameDao.save(game);
    }

}