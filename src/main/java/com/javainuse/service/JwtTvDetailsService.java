package com.javainuse.service;

import com.javainuse.DTO.TvDTO;
import com.javainuse.model.TvModel;
import com.javainuse.repository.TvRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JwtTvDetailsService {
    @Autowired
    private TvRepository tvDao;
    private long id;

    public JwtTvDetailsService() {
    }

    public TvModel save(TvDTO tv) {

        TvModel newTv = new TvModel();
        newTv.setTv(tv.getTv());
        newTv.setImage(tv.getImage());


        return tvDao.save(newTv);
    }


    public Optional<TvModel> findById(Long id) {
        return Optional.ofNullable(tvDao.findById(id));
    }


    public List<TvModel> findAll() {
        List<TvModel> tv = new ArrayList<>();
        tvDao.findAll().forEach(tv::add);
        return tv;
    }


    public void delete(Long id) {
        TvModel tv = tvDao.findById(id);
        tvDao.delete(tv);
    }


    public TvModel update(Long id) {
        TvModel tv = tvDao.findById(id);
        return tvDao.save(tv);
    }
}
