package com.javainuse.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "genre")
public class GenreModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String genre;

    @OneToMany(mappedBy = "genre")
    private Set<MovieModel> movie;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Set<MovieModel> getMovie() {
        return movie;
    }

    public void setMovie(Set<MovieModel> movie) {
        this.movie = movie;
    }
}
