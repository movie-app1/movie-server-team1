package com.javainuse.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "movie")
public class MovieModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private GenreModel genre;

    private String title;
    private String sutradara;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String cover;
    private String trailer;
    private String realease;
    private String  rating;

    public MovieModel() {

    }

    public MovieModel(long id, String title, String author, String description, String cover, String trailer, String realease, String rating) {
        this.id = id;
        this.title = title;
        this.sutradara = sutradara;
        this.description = description;
        this.cover = cover;
        this.trailer = trailer;
        this.realease = realease;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSutradara() {
        return sutradara;
    }

    public void setSutradara(String sutradara) {
        this.sutradara = sutradara;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getRealease() {
        return realease;
    }

    public void setRealease(String realease) {
        this.realease = realease;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @JsonIgnore
    public GenreModel getGenre() {
        return genre;
    }

    @JsonProperty
    public void setGenre(GenreModel genre) {
        this.genre = genre;
    }
}