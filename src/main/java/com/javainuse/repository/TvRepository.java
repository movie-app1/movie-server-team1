package com.javainuse.repository;

import com.javainuse.model.TvModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface TvRepository extends CrudRepository<TvModel, Integer> {
    TvModel findById(long id);

}
