package com.javainuse.repository;

import com.javainuse.model.MovieModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface MovieRepository extends CrudRepository<MovieModel, Integer> {
    MovieModel findById(long id);

}
