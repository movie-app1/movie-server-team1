package com.javainuse.repository;

import com.javainuse.model.GenreModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface GenreRepository extends CrudRepository<GenreModel, Integer> {
    GenreModel findById(long id);

}
