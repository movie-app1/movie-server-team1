package com.javainuse.repository;

import com.javainuse.model.GameModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository

public interface GameRepository extends CrudRepository<GameModel, Integer> {
    GameModel findById(long id);

}
