package com.javainuse.controller;

import com.javainuse.model.GenreModel;
import com.javainuse.DTO.GenreDTO;
import com.javainuse.service.JwtGenreDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class GenreController {

    public static final Logger logger = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private JwtGenreDetailsService genreDetailsService;


    // -------------------Create a Genre-------------------------------------------

    @RequestMapping(value = "/genre/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createGenre(@RequestBody GenreDTO genre) throws SQLException, ClassNotFoundException {
        logger.info("Creating Genre : {}",genre);

        genreDetailsService.save(genre);

        return new ResponseEntity<>(genre, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Genre--------------------------------------------

    @RequestMapping(value = "/genre", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<GenreModel>> listAllGenre() throws SQLException, ClassNotFoundException {

        List<GenreModel> genres = genreDetailsService.findAll();

        return new ResponseEntity<>(genres, HttpStatus.OK);
    }


    // -------------------Retrieve Single Genre By Id------------------------------------------

    @RequestMapping(value = "/genre/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getGenre(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Genre with id {}", id);

        Optional<GenreModel> genre = genreDetailsService.findById(id);

        if (genre == null) {
            logger.error("Genre with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Genre with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(genre, HttpStatus.OK);
    }

    // ------------------- Update a Genre ------------------------------------------------
    @RequestMapping(value = "/genre/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateGenre(@PathVariable("id") long id, @RequestBody GenreDTO genre) throws SQLException, ClassNotFoundException {
        logger.info("Updating Genre with id {}", id);

        Optional<GenreModel> currentGenre = genreDetailsService.findById(id);

        if (currentGenre == null) {
            logger.error("Unable to update. Genre with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Genre with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentGenre.orElseThrow().setGenre(genre.getGenre());

        genreDetailsService.update(currentGenre.get().getId());
        return new ResponseEntity<>(currentGenre, HttpStatus.OK);

    }

    // ------------------- Delete a Genre-----------------------------------------

    @RequestMapping(value = "/genre/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGenre(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Genre with id {}", id);

        genreDetailsService.delete(id);
        return new ResponseEntity<GenreModel>(HttpStatus.NO_CONTENT);
    }
}
