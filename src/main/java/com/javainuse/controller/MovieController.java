package com.javainuse.controller;

import com.javainuse.model.MovieModel;
import com.javainuse.DTO.MovieDTO;
import com.javainuse.service.JwtMovieDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class MovieController {

    public static final Logger logger = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private JwtMovieDetailsService movieDetailsService;


    // -------------------Create a Movie-------------------------------------------

    @RequestMapping(value = "/movie/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createMovie(@RequestBody MovieDTO movie) throws SQLException, ClassNotFoundException {
        logger.info("Creating Movie : {}",movie);

        movieDetailsService.save(movie);

        return new ResponseEntity<>(movie, HttpStatus.CREATED);
    }


    // -------------------Retrieve All Movie--------------------------------------------

    @RequestMapping(value = "/movie", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<MovieModel>> listAllMovie() throws SQLException, ClassNotFoundException {

        List<MovieModel> movies = movieDetailsService.findAll();

        return new ResponseEntity<>(movies, HttpStatus.OK);
    }


    // -------------------Retrieve Single Movie By Id------------------------------------------

    @RequestMapping(value = "/movie/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getMovie(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Movie with id {}", id);

        Optional<MovieModel> movie = movieDetailsService.findById(id);

        if (movie == null) {
            logger.error("Movie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Movie with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(movie, HttpStatus.OK);
    }


    // ------------------- Update a Movie ------------------------------------------------
    @RequestMapping(value = "/movie/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateMovie(@PathVariable("id") long id, @RequestBody MovieDTO movie) throws SQLException, ClassNotFoundException {
        logger.info("Updating Movie with id {}", id);

        Optional<MovieModel> currentMovie = movieDetailsService.findById(id);

        if (currentMovie == null) {
            logger.error("Unable to update. Movie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Movie with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentMovie.orElseThrow().setTitle(movie.getTitle());
        currentMovie.orElseThrow().setSutradara(movie.getSutradara());
        currentMovie.orElseThrow().setCover(movie.getCover());
        currentMovie.orElseThrow().setDescription(movie.getDescription());
        currentMovie.orElseThrow().setTrailer(movie.getTrailer());
        currentMovie.orElseThrow().setRealease(movie.getRealease());
        currentMovie.orElseThrow().setRating(movie.getRating());

        movieDetailsService.update(currentMovie.get().getId());
        return new ResponseEntity<>(currentMovie, HttpStatus.OK);

    }

    // ------------------- Delete a Movie-----------------------------------------

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMovie(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Movie with id {}", id);

        movieDetailsService.delete(id);
        return new ResponseEntity<MovieModel>(HttpStatus.NO_CONTENT);
    }

}