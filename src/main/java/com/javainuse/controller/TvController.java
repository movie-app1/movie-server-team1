package com.javainuse.controller;

import com.javainuse.DTO.TvDTO;
import com.javainuse.model.GameModel;
import com.javainuse.model.TvModel;
import com.javainuse.service.JwtTvDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class TvController {
    public static final Logger logger = LoggerFactory.getLogger(TvController.class);

    @Autowired
    private JwtTvDetailsService tvDetailsService;


    //--------------------- Create a Game ---------------------------------

    @RequestMapping(value = "/tv/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createTv(@RequestBody TvDTO tv) throws SQLException, ClassNotFoundException {
        logger.info("Creating Game : {}",tv);

        tvDetailsService.save(tv);

        return new ResponseEntity<>(tv, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Game--------------------------------------------

    @RequestMapping(value = "/tv", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<TvModel>> listAllTv() throws SQLException, ClassNotFoundException {

        List<TvModel> tvs = tvDetailsService. findAll();

        return new ResponseEntity<>(tvs, HttpStatus.OK);
    }

    // -------------------Retrieve Single Movie By Id------------------------------------------

    @RequestMapping(value = "/tv/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTv(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Tv with id {}", id);

        Optional<TvModel> Tv = tvDetailsService.findById(id);

        if (Tv == null) {
            logger.error("Tv with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Tv with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(Tv, HttpStatus.OK);
    }

    // ------------------- Update a Movie ------------------------------------------------
    @RequestMapping(value = "/tv/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTv(@PathVariable("id") long id, @RequestBody TvDTO tv) throws SQLException, ClassNotFoundException {
        logger.info("Updating Tv with id {}", id);

        Optional<TvModel> currentTv = tvDetailsService.findById(id);

        if (currentTv == null) {
            logger.error("Unable to update. Tv with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Tv with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentTv.orElseThrow().setTv(tv.getTv());
        currentTv.orElseThrow().setImage(tv.getImage());

        tvDetailsService.update(currentTv.get().getId());
        return new ResponseEntity<>(currentTv, HttpStatus.OK);

    }

    // ------------------- Delete a Movie-----------------------------------------

    @RequestMapping(value = "/tv/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletetv(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Tv with id {}", id);

        tvDetailsService.delete(id);
        return new ResponseEntity<GameModel>(HttpStatus.NO_CONTENT);
    }
}
