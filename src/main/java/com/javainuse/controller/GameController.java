package com.javainuse.controller;

import com.javainuse.DTO.GameDTO;
import com.javainuse.model.GameModel;
import com.javainuse.service.JwtGameDetailsService;
import com.javainuse.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class GameController {

    public static final Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private JwtGameDetailsService gameDetailsService;


    //--------------------- Create a Game ---------------------------------

    @RequestMapping(value = "/game/add", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createGame(@RequestBody GameDTO game) throws SQLException, ClassNotFoundException {
        logger.info("Creating Game : {}",game);

        gameDetailsService.save(game);

        return new ResponseEntity<>(game, HttpStatus.CREATED);
    }

    // -------------------Retrieve All Game--------------------------------------------

    @RequestMapping(value = "/game", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List<GameModel>> listAllGame() throws SQLException, ClassNotFoundException {

        List<GameModel> games = gameDetailsService.findAll();

        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    // -------------------Retrieve Single Movie By Id------------------------------------------

    @RequestMapping(value = "/game/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getGame(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching Game with id {}", id);

        Optional<GameModel> game = gameDetailsService.findById(id);

        if (game == null) {
            logger.error("Game with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Game with id " + id + " not found"), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(game, HttpStatus.OK);
    }

    // ------------------- Update a Movie ------------------------------------------------
    @RequestMapping(value = "/game/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateMovie(@PathVariable("id") long id, @RequestBody GameDTO game) throws SQLException, ClassNotFoundException {
        logger.info("Updating Movie with id {}", id);

        Optional<GameModel> currentGame = gameDetailsService.findById(id);

        if (currentGame == null) {
            logger.error("Unable to update. Movie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to update. Game with id " + id + " not found."), HttpStatus.NOT_FOUND);
        }
        currentGame.orElseThrow().setGame(game.getGame());
        currentGame.orElseThrow().setImage(game.getImage());

        gameDetailsService.update(currentGame.get().getId());
        return new ResponseEntity<>(currentGame, HttpStatus.OK);

    }

    // ------------------- Delete a Movie-----------------------------------------

    @RequestMapping(value = "/game/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletegame(@PathVariable("id") long id) throws SQLException, ClassNotFoundException {
        logger.info("Fetching & Deleting Game with id {}", id);

        gameDetailsService.delete(id);
        return new ResponseEntity<GameModel>(HttpStatus.NO_CONTENT);
    }
}
